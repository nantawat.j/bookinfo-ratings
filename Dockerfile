FROM node:14

WORKDIR /usr/src/app/

COPY src/ /usr/src/app/

RUN npm install

CMD [ "node", "/usr/src/app/ratings.js","8080" ]