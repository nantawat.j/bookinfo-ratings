# How to run ratings service

## Prerequisite

* Node 14

```bash
npm install
node ratings.js 8080
```


##test with
```
docker-compose up --build
```

```
http://localhost:8080/health

http://localhost:8080/ratings/1
```